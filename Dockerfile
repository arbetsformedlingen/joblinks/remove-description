FROM docker.io/library/ubuntu:22.04 AS base

WORKDIR /app

COPY requirements.txt requirements.txt
COPY main.py run.sh /app/

COPY . /app

RUN apt-get -y update \
    && apt-get -y install python3 python3-pip \
    && python3 -m pip install -r requirements.txt

CMD ["python3","main.py"]
